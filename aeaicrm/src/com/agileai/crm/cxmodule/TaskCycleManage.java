package com.agileai.crm.cxmodule;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface TaskCycleManage
        extends StandardService {
	DataRow getTaskReviewIdRecord(DataParam param);
}

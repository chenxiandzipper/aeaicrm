package com.agileai.crm.cxmodule;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface OppInfoManage
        extends StandardService {

	void createOrderRecord(DataParam param);
	void changeStateRecord(DataParam param);
	void changeStateRecord(String oppId);
	void createContRecord(DataParam param);
	DataRow getCustStateRecord(DataParam param);
}

package com.agileai.crm.module.taskreview.service;

import java.util.Date;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.util.DateUtil;

public class TaskReviewManageImpl
        extends StandardServiceImpl
        implements TaskReviewManage {
    public TaskReviewManageImpl() {
        super();
    }

	@Override
	public DataRow retrieveCurrentWeekRow() {
		DataRow result = null;
		Date date = new Date();
		Date currentWeekStartDay = DateUtil.getBeginOfWeek(date);
		Date lastWeekEndDay = DateUtil.getDateAdd(currentWeekStartDay,DateUtil.DAY, -1);
		DataParam param = new DataParam("lastWeekEndDay",lastWeekEndDay);
		String statementId = sqlNameSpace+"."+"findCycleRecords";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null && records.size() > 0){
			result = records.get(0);
		}
		return result;
	}

	@Override
	public DataRow getWeekRow(String weekId) {
		DataRow result = null;
		DataParam param = new DataParam("TC_ID",weekId);
		String statementId = sqlNameSpace+"."+"getRecord";
		result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
	
	@Override
	public DataRow getBeforeWeekRow(String tcId) {
		DataRow result = null;
		DataParam param = new DataParam("TC_ID",tcId);
		String statementId = sqlNameSpace+"."+"getBeforeWeekRow";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null && records.size() > 0){
			result = records.get(0);
		}
		return result;
	}

	@Override
	public DataRow getNextWeekRow(String tcId) {
		DataRow result = null;
		DataParam param = new DataParam("TC_ID",tcId);
		String statementId = sqlNameSpace+"."+"getNextWeekRow";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null && records.size() > 0){
			result = records.get(0);
		}
		return result;
	}

	@Override
	public List<DataRow> querySaleRecords() {
		String statementId = sqlNameSpace+"."+"querySaleRecords";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, new DataParam());
		return records;
	}
}

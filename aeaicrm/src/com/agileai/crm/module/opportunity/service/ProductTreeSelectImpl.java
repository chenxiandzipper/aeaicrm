package com.agileai.crm.module.opportunity.service;

import com.agileai.crm.module.opportunity.service.ProductTreeSelect;
import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;

public class ProductTreeSelectImpl
        extends TreeSelectServiceImpl
        implements ProductTreeSelect {
    public ProductTreeSelectImpl() {
        super();
    }
}

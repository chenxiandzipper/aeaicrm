<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>商机管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var custIdBox;
function openCustIdBox(){
	var handlerId = "CustomerListSelectList"; 
	if (!custIdBox){
		custIdBox = new PopupBox('custIdBox','请选择客户',{size:'big',height:'500px',width:'700px',top:'2px'});  
	}
	var url = 'index?'+handlerId+'&targetId=CUST_ID&targetName=CUST_ID_NAME';
	custIdBox.sendRequest(url);
} 
var contIdBox;
function openContIdBox(){
	var handlerId = "ContListSelectList"; 
	if (!contIdBox){
		contIdBox = new PopupBox('contIdBox','请选择联系人',{size:'normal',height:'500px',width:'700px',top:'2px'});  
	}
	var url = 'index?'+handlerId+'&targetId=CONT_ID&targetName=CONT_ID_NAME&custId='+$('#CUST_ID').val();
	contIdBox.sendRequest(url);
} 
var editBox;
function showEditBox(){
	clearSelection();
	var handlerId = "OppCreateContEdit"; 
	if (!editBox){
		editBox = new PopupBox('editBox','新增联系人',{size:'big',height:'350px',width:'600px',top:'30px'});
	}
	var url = 'index?'+handlerId+'&custId='+ele('CUST_ID').value+'&operaType=insert';
	editBox.sendRequest(url);
}
var saleIdBox;
function openSaleIdBox(){
	var handlerId = "SalesmanListSelectList"; 
	if (!saleIdBox){
		saleIdBox = new PopupBox('saleIdBox','请选择销售',{size:'normal',height:'500px',width:'700px',top:'2px'});  
	}
	var url = 'index?'+handlerId+'&targetId=CLUE_SALESMAN&targetName=CLUE_SALESMAN_NAME';
	saleIdBox.sendRequest(url);
} 
var proIdBox;
function openProIdBox(){
	var handlerId = "ProductTreeSelect"; 
	if (!proIdBox){
		proIdBox = new PopupBox('proIdBox','请选择方案',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=OPP_CONCERN_PRODUCT&targetName=OPP_CONCERN_PRODUCT_NAME';
	proIdBox.sendRequest(url);
}
function doClose(){
	jConfirm('您确认是要关闭数据吗',function(r){
		if(r){
			doSubmit({actionType:'doClose'});
		}
	});
}
function doSave(){
	doSubmit({actionType:'save'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
 <%if(pageBean.getBoolValue("doEdit8Save")){ %>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td></aeai:previlege>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSave()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
  <%} %>
   <%if(pageBean.getBoolValue("doSubmit")){ %>
   <aeai:previlege code="submit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'doSubmit'})"><input value="&nbsp;" type="button" class="submitImgBtn"  title="提交" />提交</td></aeai:previlege>
 <%} %>
 <%if(pageBean.getBoolValue("doRevokeSubmit")){%>
  <aeai:previlege code="resubmit"><td  align="center" class="bartdx" onclick="doSubmit({actionType:'doRevokeSubmit'})" onmouseover="onMover(this);" onmouseout="onMout(this);"><input value="&nbsp;" type="button" class="reSubmittedImgBtn" title="反提交" />反提交</td></aeai:previlege>
 <%}%> 
 <%if(pageBean.getBoolValue("doConfirm")){ %>
   <aeai:previlege code="confirm"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'doConfirm'})"><input value="&nbsp;" type="button" class="confirmImgBtn"  title="提交" />确认</td></aeai:previlege>
   <%} %>
   <%if(pageBean.getBoolValue("doRevokeConfirm")){ %>
   <aeai:previlege code="reConfirm"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'doRevokeConfirm'})"><input value="&nbsp;" type="button" class="revokeConfirmImgBtn"  title="反确认" />反确认</td></aeai:previlege>
 <%} %>
 <%if(pageBean.getBoolValue("doClose")){ %>
  <aeai:previlege code="close"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doClose();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td></aeai:previlege>
   <%} %>
  <aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>商机名称</th>
	<td><input id="OPP_NAME" label="商机名称" name="OPP_NAME" type="text" value="<%=pageBean.inputValue("OPP_NAME")%>" size="24" style="width:302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>级别</th>
	<td>
    <select id="OPP_LEVEL" label="级别" name="OPP_LEVEL" style="width: 303px" class="select"><%=pageBean.selectValue("OPP_LEVEL")%></select>
</td>
</tr>
<tr>
	<input id="CLUE_ID" label="线索名称" name="CLUE_ID" type="hidden" value="<%=pageBean.inputValue("CLUE_ID")%>"/>
</tr>
<tr>
	<th width="100" nowrap>联系人名称</th>
	<td>
	<input id="CONT_ID_NAME" label="联系人姓名" name="CONT_ID_NAME" type="text" value="<%=pageBean.inputValue("CONT_ID_NAME")%>" readonly="readonly" size="24" style="width:302px" class="text" />
	 <%if(!pageBean.getBoolValue("readOnly")){ %>
	<img id="contIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openContIdBox()" />
<!-- 	<label onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="showEditBox()"> -->
<!-- 	<input value="&nbsp;" type="button" class="editImgBtn" title="新增" />新增</label> -->
	<%} %>
	<input id="CONT_ID" name="CONT_ID" type="hidden" value="<%=pageBean.inputValue("CONT_ID")%>"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>跟进人员</th>
	<td><input name="CLUE_SALESMAN_NAME" type="text" class="text"	id="CLUE_SALESMAN_NAME"	value="<%=pageBean.inputValue("CLUE_SALESMAN_NAME")%>" size="24" style="width:302px"	readonly="readonly" label="跟进人员" /> 
		 <%if(pageBean.getBoolValue("doAssign")){ %>
		<img id="saleIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openSaleIdBox()" />
		<%} %>
		<input id="CLUE_SALESMAN" label="跟进人员" name="CLUE_SALESMAN" type="hidden" value="<%=pageBean.inputValue("CLUE_SALESMAN")%>" size="24" class="text" />
	</td>
</tr>
<tr>
	<th width="100" nowrap>商机说明</th>
	<td><textarea id="OPP_DES" label="商机说明" name="OPP_DES" cols="40" rows="3" style="width:540px" class="textarea"><%=pageBean.inputValue("OPP_DES")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>关注产品</th>
	<td><input id="OPP_CONCERN_PRODUCT" label="关注产品" name="OPP_CONCERN_PRODUCT" type="text" value="<%=pageBean.inputValue("OPP_CONCERN_PRODUCT")%>" size="24" style="width:302px" class="text" readonly="readonly" />
		<img id="proIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openProIdBox()"/>
	</td>
</tr>
<tr>
	<th width="100" nowrap>金额</th>
	<td><input id="OPP_EXPECT_INVEST" label="预期投入" name="OPP_EXPECT_INVEST" type="text" value="<%=pageBean.inputValue("OPP_EXPECT_INVEST")%>" size="24" style="width:302px" class="text" />
(单位：万)</td>
</tr>
<tr>
	<th width="100" nowrap>启动时间</th>
	<td><input id="OPP_START_TIME" label="启动时间" name="OPP_START_TIME" type="text" value="<%=pageBean.inputDate("OPP_START_TIME")%>" readonly="readonly" size="24" style="width:302px" class="text" /><img id="OPP_START_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>
	<input id="OPP_STATE_TEXT" label="状态" name="OPP_STATE_TEXT" type="text" value="<%=pageBean.selectedText("OPP_STATE")%>" size="24" style="width:302px" class="text" readonly="readonly"/>
	<input id="OPP_STATE" label="状态" name="OPP_STATE" type="hidden" value="<%=pageBean.selectedValue("OPP_STATE")%>" />
   </td>
</tr>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="OPP_CREATER_NAME" type="text" class="text"	id="OPP_CREATER_NAME"	value="<%=pageBean.inputValue("OPP_CREATER_NAME")%>" size="24" style="width:302px" readonly="readonly" label="创建人" /> 
		<input id="OPP_CREATER" label="创建人"  name="OPP_CREATER" type="hidden"	value="<%=pageBean.inputValue("OPP_CREATER")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input id="OPP_CREATE_TIME" label="创建时间" name="OPP_CREATE_TIME" type="text" value="<%=pageBean.inputTime("OPP_CREATE_TIME")%>" readonly="readonly" size="24" style="width:302px" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="OPP_ID" name="OPP_ID" value="<%=pageBean.inputValue("OPP_ID")%>" />
<input type="hidden" id="CUST_ID" name="CUST_ID" value="<%=pageBean.inputValue("CUST_ID")%>" />
</form>
<script language="javascript">
$('#OPP_DES').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
initCalendar('OPP_START_TIME','%Y-%m-%d','OPP_START_TIMEPicker');
numValidator.add("OPP_EXPECT_INVEST");
requiredValidator.add("OPP_LEVEL");
requiredValidator.add("OPP_NAME");
requiredValidator.add("OPP_DES");
requiredValidator.add("OPP_CONCERN_PRODUCT");
datetimeValidators[0].set("yyyy-MM-dd").add("OPP_START_TIME");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
